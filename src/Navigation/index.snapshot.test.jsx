import React from "react";
import { render } from "react-testing-library";

import Navigation from "./index";

const items = [
  {
    item: "Home",
    active: true,
  },
  {
    Component: <div>hello</div>,
  },
];

describe("Navigation", () => {
  it("should be rendered", () => {
    const { container } = render(<Navigation tabs={items} />);

    expect(container.firstChild).toMatchSnapshot();
  });
});
