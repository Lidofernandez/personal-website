import React from "react";
import PropTypes from "prop-types";

import Button from "../Button";
import Tabs from "../Tabs";
import logo from "./logo.png";
import "./index.css";

// This is a very simple implementation
// I would rather set page name via
// the router and not hard coded
const Navigation = ({ tabs }) => (
  <nav className="navigation__container">
    <div className="navigation__container-brand">
      <Button icon="menu" className="navigation__action-button" />
      <img src={logo} alt="logo" className="navigation__image" />
    </div>
    <span className="navigation__item--is-active">Home</span>
    <Tabs tabs={tabs} className="navigation__items" />
  </nav>
);

Navigation.propTypes = {
  tabs: PropTypes.arrayOf(PropTypes.any),
};

Navigation.defaultProps = {
  tabs: [],
};

export default Navigation;
