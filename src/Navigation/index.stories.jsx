import React from "react";
import { storiesOf } from "@storybook/react";

import Navigation from "./index";
import SearchBox from "../SearchBox";

const items = [
  {
    item: "Home",
    active: true,
  },
  {
    item: "Features",
  },
  {
    item: "Portfolio",
  },
  {
    item: "Blog",
  },
  {
    Component: <SearchBox showToogleIcon placeholder="search" />,
  },
];

storiesOf("Navigation", module).add("Simple", () => (
  <Navigation tabs={items} />
));
