// This is a public API from https://pixabay.com/api/docs/#
const API_KEY = "1433238-84121e368d814118a9d456058";

const params = (q, perPage = 5) => ({
  key: API_KEY,
  q,
  image_type: "photo",
  per_page: perPage,
});

const queryParams = (query, limit) =>
  Object.entries(params(query, limit))
    .map(([key, value]) => `${key}=${value}`)
    .join("&");

export default (query, limit) =>
  fetch(`https://pixabay.com/api/?${queryParams(query, limit)}`)
    .then(response => response.json())
    .then(data => data);
