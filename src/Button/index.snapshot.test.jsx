import React from "react";
import { render } from "react-testing-library";

import Button from "./index";

describe("Button", () => {
  describe("With Icon", () => {
    it("should be rendered", () => {
      const { container } = render(<Button icon="test" />);

      expect(container.firstChild).toMatchSnapshot();
    });
  });
});
