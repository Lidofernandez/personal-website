// This is a reducer because I don't want to "hard code"
// the API specific naming convention
export default items =>
  items.map(({ largeImageURL, ...rest }) => ({
    ...rest,
    src: largeImageURL,
  }));
