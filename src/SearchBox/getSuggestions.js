export default query =>
  fetch(`http://localhost:3000/suggestions?q=${query}`)
    .then(response => response.json())
    .then(data => data);
