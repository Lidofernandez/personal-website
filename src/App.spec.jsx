import ReactDOM from "react-dom";
import React from "react";
import { render, fireEvent, waitForElement } from "react-testing-library";

import App from "./App";

const handlOnChange = value => ({
  target: { value },
});

const data = response =>
  new Promise(resolve =>
    resolve({
      json() {
        return response;
      },
    }),
  );

describe("App", () => {
  it("renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<App />, div);
  });
  beforeEach(() => {
    global.fetch = jest.fn().mockImplementation(apiUrl => {
      if (apiUrl.startsWith("https://pixabay.com/api")) {
        return data({
          hits: [
            { largeImageURL: "Chuck Norris at this best" },
            { largeImageURL: "Antonio Banderas just trying" },
          ],
        });
      }

      return data({
        articles: [
          { name: "Chuck Norris", description: "Is the best" },
          { name: "Antonio Banderas", description: "Could be better" },
        ],
      });
    });
  });

  it("should be rendered", async () => {
    const { container, getByTestId } = render(<App />);
    await waitForElement(() => getByTestId("Chuck Norris at this best"));
    expect(container.firstChild).toMatchSnapshot();
  });

  it("should render a list by article", async () => {
    const { getByTestId, getByPlaceholderText } = render(<App />);
    fireEvent.click(getByTestId("toogleIcon"));
    const search = getByPlaceholderText("Zoeken");
    const inputText = "Chuck Norris";
    fireEvent.change(search, handlOnChange(inputText));
    fireEvent.click(getByTestId("submit"));
    const articleName = await waitForElement(() => getByTestId("Chuck Norris"));
    await waitForElement(() => getByTestId("Chuck Norris at this best"));
    expect(articleName).toHaveTextContent("Is the best");
  });
});
