import React, { PureComponent, Fragment } from "react";
import PropTypes from "prop-types";

import getImages from "../services/getImages";
import Button from "../Button";
import transformImages from "../transformers/images";
import "./index.css";

class Carousel extends PureComponent {
  constructor(props) {
    super(props);

    this.state = { activeIndex: 0, items: [] };
  }

  // For simplicity I'm not separating
  // the UI from the data
  componentDidMount = async () => {
    const { interval, query } = this.props;
    const { hits } = await getImages(query);

    const items = transformImages(hits);
    this.setState({
      items,
    });

    this.intervalId = setInterval(
      () => this.setState({ activeIndex: this.randomIndex() }),
      interval,
    );
  };

  componentWillUnmount() {
    clearInterval(this.intervalId);
  }

  handleClick = index => {
    clearInterval(this.intervalId);
    this.setState({ activeIndex: index });
  };

  itemCount() {
    const { items } = this.state;
    return items.length;
  }

  randomIndex() {
    return Math.floor(Math.random() * this.itemCount());
  }

  renderItems() {
    const { activeIndex, items } = this.state;
    if (items.length < 1) {
      return null;
    }

    return items.map(({ src, tags }, index) => (
      <img
        data-testid={src}
        key={src}
        src={src}
        alt={tags}
        className={`carousel__image ${
          index === activeIndex ? "carousel__image--is-active" : null
        }`}
      />
    ));
  }

  renderIndicators() {
    const { activeIndex } = this.state;
    const totalCount = this.itemCount();
    const nextIndex = (activeIndex + 1) % totalCount;
    const previousIndex = (totalCount + activeIndex - 1) % totalCount;
    return (
      <Fragment>
        <Button
          className="carousel__action-button carousel__action-button--is-left"
          role="button"
          tabIndex="0"
          icon="arrow_back"
          onClick={() => this.handleClick(previousIndex)}
        />
        <Button
          className="carousel__action-button carousel__action-button--is-right"
          role="button"
          tabIndex="0"
          icon="arrow_forward"
          onClick={() => this.handleClick(nextIndex)}
        />
      </Fragment>
    );
  }

  render() {
    return (
      <div className="carousel__container">
        <div className="carousel__slides">{this.renderItems()}</div>
        <div className="carousel__action-container">
          {this.itemCount() > 1 ? this.renderIndicators() : null}
        </div>
      </div>
    );
  }
}

Carousel.propTypes = {
  interval: PropTypes.number,
  query: PropTypes.string,
};

Carousel.defaultProps = {
  interval: 1000,
  query: "",
};

export default Carousel;
