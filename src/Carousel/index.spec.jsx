import React from "react";
import { render, waitForElement, fireEvent } from "react-testing-library";

import Carousel from "./index";

const waitFor = (time = 0) => new Promise(resolve => setTimeout(resolve, time));

describe("Carousel", () => {
  beforeEach(() => {
    global.fetch = jest.fn().mockImplementation(
      () =>
        new Promise(resolve =>
          resolve({
            json() {
              return {
                hits: [
                  { largeImageURL: "1" },
                  { largeImageURL: "2" },
                  { largeImageURL: "3" },
                  { largeImageURL: "4" },
                ],
              };
            },
          }),
        ),
    );
  });

  it("should show a random image", async () => {
    const { container, getByTestId } = render(<Carousel interval={100} />);
    await waitForElement(() => getByTestId("1"));
    // This is useful to trigger the randomIndex() method
    // It needs some time to populate the new class
    await waitFor(1000);
    const { src } = container.querySelector(".carousel__image--is-active");
    expect(Number(src.split("/")[3])).toBeGreaterThan(0);
  });

  it("should show the next slide", async () => {
    const { container, getByTestId, getByText } = render(<Carousel />);
    await waitForElement(() => getByTestId("1"));
    fireEvent.click(getByText("arrow_forward"));

    const { src } = container.querySelector(".carousel__image--is-active");
    expect(src).toContain("2");
  });

  it("should show the previous slide", async () => {
    const { container, getByTestId, getByText } = render(<Carousel />);
    await waitForElement(() => getByTestId("1"));
    fireEvent.click(getByText("arrow_back"));

    const { src } = container.querySelector(".carousel__image--is-active");
    expect(src).toContain("4");
  });
});
