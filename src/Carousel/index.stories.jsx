import React from "react";
import { storiesOf } from "@storybook/react";

import Carousel from "./index";

storiesOf("Carousel", module).add("Simple", () => (
  <Carousel interval={1500} query="tigers" />
));
