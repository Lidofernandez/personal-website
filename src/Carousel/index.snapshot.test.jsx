import React from "react";
import { render, waitForElement } from "react-testing-library";

import Carousel from "./index";

describe("Carousel", () => {
  it("should be rendered", () => {
    const { container } = render(<Carousel />);
    expect(container.firstChild).toMatchSnapshot();
  });

  beforeEach(() => {
    global.fetch = jest.fn().mockImplementation(
      () =>
        new Promise(resolve =>
          resolve({
            json() {
              return {
                hits: [
                  { largeImageURL: "Chuck Norris" },
                  { largeImageURL: "Antonio Banderas" },
                ],
              };
            },
          }),
        ),
    );
  });

  it("should be rendered with data", async () => {
    const { container, getByTestId } = render(<Carousel />);
    await waitForElement(() => getByTestId("Chuck Norris"));
    expect(container.firstChild).toMatchSnapshot();
  });
});
