import React from "react";
import PropTypes from "prop-types";

import "./index.css";

const Icon = ({ icon, className, ...props }) => (
  <i className={`material-icons icon ${className}`} {...props}>
    {icon}
  </i>
);

Icon.propTypes = {
  /** sets the icon name */
  icon: PropTypes.string,
  /** sets the class name */
  className: PropTypes.string,
};

Icon.defaultProps = {
  icon: undefined,
  className: "",
};

export default Icon;
