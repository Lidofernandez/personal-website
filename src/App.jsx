import React, { PureComponent } from "react";
import PropTypes from "prop-types";

import SearchBox from "./SearchBox";
import getArticles from "./getArticles";
import Carousel from "./Carousel";
import suggestionPattern from "./lib/suggestionPattern";
import Button from "./Button";
import Navigation from "./Navigation";
import Tabs from "./Tabs";
import logo from "./fc_barcelona.svg";
import getImages from "./services/getImages";
import transformImages from "./transformers/images";
import Icon from "./Icon";
import TextField from "./TextField";
import "./index.css";
// This could be set as an enviroment variable
// so I don't have to make an extra commit to update
// this value
const MINIMUN_VALUE = 2;

// Tabs can come from an API
const TABS = [
  {
    item: "All",
    active: true,
  },
  {
    item: "Business",
  },
  {
    item: "Creative",
  },
  {
    item: "Portfolio",
  },
  {
    item: "Infrastructure",
  },
  {
    item: "GLobal",
  },
  {
    item: "Industrial",
  },
];

/* eslint-disable max-len */
const CONTENT_DATA = [
  {
    icon: "accessibility",
    title: "hello",
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets contans of Lorem Ipsum.",
  },
  {
    icon: "3d_rotation",
    title: "hello",
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets contans of Lorem Ipsum.",
  },
  {
    icon: "accessible",
    title: "hello",
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets contans of Lorem Ipsum.",
  },
  {
    icon: "account_circle",
    title: "hello",
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets contans of Lorem Ipsum.",
  },
  {
    icon: "android",
    title: "hello",
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets contans of Lorem Ipsum.",
  },
  {
    icon: "alarm_add",
    title: "hello",
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets contans of Lorem Ipsum.",
  },
];
/* eslint-enable max-len */

const Header = ({ children }) => (
  <div className="container--header">
    <div>
      <img src={logo} alt="logo" />
      {children}
    </div>
  </div>
);

Header.propTypes = {
  children: PropTypes.node,
};

Header.defaultProps = {
  children: null,
};

// I've decided to keep this component only for the props
// related with page transitions or initial setups

class App extends PureComponent {
  state = {
    filteredArticles: [],
    items: [],
  };

  handleOnSubmit = async result => {
    const { articles } = await getArticles(result);
    const filteredArticles = articles.filter(({ name }) =>
      // As I don't access to query to a real db this is how
      // I fixed it
      suggestionPattern(result).test(name),
    );
    this.setState({ filteredArticles });
  };

  componentDidMount = async () => {
    const { hits } = await getImages("koalas", 3);

    const items = transformImages(hits);
    this.setState({
      items,
    });
  };

  renderArticles() {
    // As this is not part of the current project It
    // won't be populated
    const { filteredArticles } = this.state;

    const list = filteredArticles.map(({ name, description }) => (
      <li className="li" data-testid={name} key={name}>
        {/* Here I would implement a similar form as I did for the search
            with a onSubmit={this.handleOnDelete} for example
          */}
        {name} - {description} <Button icon="delete" />
      </li>
    ));

    return list.length ? <ul data-testid="resuls">{list}</ul> : null;
  }

  renderNavigation() {
    const items = [
      {
        item: "Home",
        active: true,
      },
      {
        item: "Features",
      },
      {
        item: "Portfolio",
      },
      {
        item: "Blog",
      },
      {
        Component: (
          <SearchBox
            showToogleIcon
            onSubmit={this.handleOnSubmit}
            minimumSearchValue={MINIMUN_VALUE}
            placeholder="Zoeken"
          />
        ),
      },
    ];

    return <Navigation tabs={items} />;
  }

  renderImageGrid() {
    const { items } = this.state;
    return (
      <div className="grid-container">
        {items.map(({ src, alt }) => (
          <div key={src} className="grid-container--cell">
            {<img className="images--grid-cell" src={src} alt={alt} />}
          </div>
        ))}
      </div>
    );
  }

  static renderFooter() {
    return (
      <div className="footer__container">
        <Header>
          <div className="footer__container__subtitle">
            Address: World center
          </div>
        </Header>
        <form className="footer__contact-form">
          <h3 className="footer__container__subtitle">Contact us</h3>
          <TextField placeholder="Your name" />
          <TextField placeholder="Your email" type="email" />
          <div className="text-field text-field__textarea">
            <textarea
              className="text-field__input"
              placeholder="Your message"
              rows="5"
              cols="20"
            />
          </div>
          <div className="text-field">
            <Button className="text-field__input action-button" type="submit">
              Submit
            </Button>
          </div>
        </form>
      </div>
    );
  }

  static renderContentGrid() {
    return (
      <div className="grid-container">
        {CONTENT_DATA.map(({ icon, title, description }) => (
          <div key={icon}>
            <Icon icon={icon} />
            <h3>{title}</h3>
            <p>{description}</p>
          </div>
        ))}
      </div>
    );
  }

  render() {
    return (
      <div className="container">
        {this.renderNavigation()}
        {this.renderArticles()}
        <Header>
          <div>FC Barcelona</div>
        </Header>
        <div className="content">
          <Carousel interval={1500} query="tigers" />
        </div>
        <h2>Our Projects are the best</h2>
        <Tabs tabs={TABS} vertical />
        {this.renderImageGrid()}
        <h2>Our Projects are really the best</h2>
        {App.renderContentGrid()}
        {App.renderFooter()}
      </div>
    );
  }
}
export default App;
