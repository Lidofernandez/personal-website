import React from "react";
import { render, fireEvent } from "react-testing-library";

import Suggestions from "./index";

const props = {
  suggestions: [{ searchterm: "Chuck Norris" }, { searchterm: "Bruce Lee" }],
};

describe("Suggestions", () => {
  it("should highlight the matching suggestion", () => {
    const { container } = render(
      <Suggestions {...props} highlightSuggestion="Bruce" />,
    );

    const isHighlighted = container.querySelector(".isHighlighted");
    expect(isHighlighted).toHaveTextContent("Bruce");
  });
  it("should clicked the selected value", () => {
    const handleOnClick = jest.fn();
    const { getByTestId } = render(
      <Suggestions {...props} onClick={handleOnClick} />,
    );

    fireEvent.click(getByTestId("Chuck Norris"));
    expect(handleOnClick).toHaveBeenCalledTimes(1);
    expect(handleOnClick).toHaveBeenCalledWith("Chuck Norris");
  });
});
