import React from "react";
import { storiesOf } from "@storybook/react";

import Tabs from "./index";
import SearchBox from "../SearchBox";

const items = [
  {
    item: "Home",
    active: true,
  },
  {
    item: "Features",
  },
  {
    item: "Portfolio",
  },
  {
    item: "Blog",
  },
  {
    Component: <SearchBox showToogleIcon placeholder="search" />,
  },
];

storiesOf("Tabs", module)
  .add("Simple", () => <Tabs tabs={items} />)
  .add("With vertical", () => <Tabs tabs={items} vertical />);
