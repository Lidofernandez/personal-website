import React from "react";
import PropTypes from "prop-types";
import Icon from "../Icon";

import "./index.css";

const Tabs = ({ tabs, className, vertical }) => (
  <ul
    className={`tabs__items ${className} ${
      vertical ? "tabs--is-vertical" : null
    }`}
  >
    {tabs.map(({ item, active, icon, Component }) => {
      const content = item || (icon && <Icon icon={icon} />) || Component;
      return (
        <li
          key={content}
          className={`tabs__items--item ${
            active ? "tabs__items-item--is-active" : null
          }`}
        >
          {content}
        </li>
      );
    })}
  </ul>
);

Tabs.propTypes = {
  tabs: PropTypes.arrayOf(PropTypes.any).isRequired,
  className: PropTypes.string,
  vertical: PropTypes.bool,
};

Tabs.defaultProps = {
  className: "",
  vertical: false,
};

export default Tabs;
