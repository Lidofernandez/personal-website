import React from "react";
import { render } from "react-testing-library";

import Tabs from "./index";

const tabs = [
  {
    item: "Home",
    active: true,
  },
  {
    item: "hello",
  },
  {
    Component: <div>hello</div>,
  },
];

describe("Tabs", () => {
  it("should be rendered", () => {
    const { container } = render(<Tabs tabs={tabs} />);

    expect(container.firstChild).toMatchSnapshot();
  });
});
