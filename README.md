# personal-website

## How to run this project
This project is based on react-scripts

```sh
npm run start
```

## How to run tests
This project uses jest and react-testing-library

```sh
npm run test
```
